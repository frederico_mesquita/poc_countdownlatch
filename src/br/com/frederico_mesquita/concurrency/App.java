package br.com.frederico_mesquita.concurrency;

import java.util.concurrent.CountDownLatch;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class App {
	
	static int NUMBER_OF_THREADS = 5;

	public static void main(String[] args) {
		ExecutorService executorService = Executors.newSingleThreadExecutor();
		CountDownLatch latch = new CountDownLatch(NUMBER_OF_THREADS);
		
		for(int count = 1; count <= NUMBER_OF_THREADS; count++) {
			executorService.execute(Worker.getInstance(count, latch));
		}
		
		try {
			latch.await();
		}catch (InterruptedException e) {
			e.printStackTrace();
		}
		
		System.out.println("All prerequisites are done!!");
		executorService.shutdown();
	}

}

class Worker implements Runnable{
	
	private int id;
	private CountDownLatch countDownLatch;
	
	private Worker(int id, CountDownLatch countDownLatch) {
		this.id = id;
		this.countDownLatch = countDownLatch;
	}
	
	public static Worker getInstance(int id, CountDownLatch countDownLatch) {
		return new Worker(id, countDownLatch);
	}

	public void run() {
		doWork();
		countDownLatch.countDown();
		System.out.println("Thread with id " + this.id + " ends working...");
	}

	private void doWork() {
		System.out.println("Thread with id " + this.id + " starts working...");
		try {
			Thread.sleep((5 - this.id) * 1000);
		}catch (InterruptedException e) {
			 e.printStackTrace();
		}
	}
}

